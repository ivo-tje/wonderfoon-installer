#!/bin/bash

if [ ! -n "$BASH" ] ;then echo Please run this script $0 with bash; exit 1; fi

if [ "$EUID" -ne 0 ]
  then echo "Please run as root using sudo"
  exit
fi

cd /home/pi
apt update
apt -y upgrade
apt -y install python3-pip mpg123 espeak dnsmasq hostapd
mkdir -p /usr/share/wonderfoon/music/
chown -R pi /usr/share/wonderfoon
mkdir /etc/wonderfoon
chown pi /etc/wonderfoon

#install wonderfoon-python
git clone https://gitlab.com/ivo-tje/wonderfoon-python.git
cd wonderfoon-python
pip3 install -r requirements.txt
cp wonderfoon-python.service /etc/systemd/system/
cp dial.mp3 /usr/share/wonderfoon

#install wonderfoon-webinterface
cd /home/pi
git clone https://gitlab.com/ivo-tje/wonderfoon-webinterface.git
cd wonderfoon-webinterface
pip3 install -r requirements.txt
cp wonderfoon-webinterface.service /etc/systemd/system/
cp hostapd.conf /etc/hostapd/hostapd.conf

#setup wifi hotspot
echo "192.168.4.1	wonderfoon.localnet wonderfoon" >> /etc/hosts
echo "local=/localnet/" >> /etc/dnsmasq.conf
echo "domain=localnet" >> /etc/dnsmasq.conf
echo "interface=wlan0" >> /etc/dnsmasq.conf
echo "  dhcp-range=192.168.4.2,192.168.4.20,255.255.255.0,24h" >> /etc/dnsmasq.conf
MAC=`ip l show wlan0 | grep ether | cut -d ' ' -f 6 | sed 's/://g'`
sed -i "/^ssid/c\ssid=WF-$MAC" /etc/hostapd/hostapd.conf
echo 'DAEMON_CONF="/etc/hostapd/hostapd.conf"' >> /etc/default/hostapd

sed -i "/exit 0/d" /etc/rc.local
echo "MAC=\`ip l show wlan0 | grep ether | cut -d ' ' -f 6 | sed 's/://g'\`" >> /etc/rc.local
echo "sed -i \"/^ssid/c\ssid=WF-\$MAC\" /etc/hostapd/hostapd.conf" >> /etc/rc.local
echo "systemctl restart hostapd" >> /etc/rc.local
echo "exit 0" >> /etc/rc.local

# Wifi settings.
rm /etc/wpa_supplicant/wpa_supplicant.conf

cat << EOF > /etc/wpa_supplicant/wpa_supplicant.conf
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1

network={
       ssid="Wonderfoon"
       psk="Wonderfoon"
       key_mgmt=WPA-PSK
}
EOF

cat << EOF > /etc/dhcpcd.conf
hostname
clientid
persistent
option rapid_commit
option domain_name_servers, domain_name, domain_search, host_name
option classless_static_routes
option ntp_servers
option interface_mtu
require dhcp_server_identifier
slaac private
interface wlan0
    nohook wpa_supplicant
EOF

cat << 'EOF' > /usr/bin/autohotspot
#!/bin/bash
#version 0.95-4-N/HS

#You may share this script on the condition a reference to RaspberryConnect.com
#must be included in copies or derivatives of this script.

#A script to switch between a wifi network and a non internet routed Hotspot
#Works at startup or with a seperate timer or manually without a reboot
#Other setup required find out more at
#http://www.raspberryconnect.com

wifidev="wlan0" #device name to use. Default is wlan0.
#use the command: iw dev ,to see wifi interface name

IFSdef=$IFS
cnt=0
#These four lines capture the wifi networks the RPi is setup to use
wpassid=$(awk '/ssid="/{ print $0 }' /etc/wpa_supplicant/wpa_supplicant.conf | awk -F'ssid=' '{ print $2 }' ORS=',' | sed 's/\"/''/g' | sed 's/,$//')
IFS=","
ssids=($wpassid)
IFS=$IFSdef #reset back to defaults


#Note:If you only want to check for certain SSIDs
#Remove the # in in front of ssids=('mySSID1'.... below and put a # infront of all four lines above
# separated by a space, eg ('mySSID1' 'mySSID2')
#ssids=('Wonderfoon')

#Enter the Routers Mac Addresses for hidden SSIDs, seperated by spaces ie
#( '11:22:33:44:55:66' 'aa:bb:cc:dd:ee:ff' )
mac=()

ssidsmac=("${ssids[@]}" "${mac[@]}") #combines ssid and MAC for checking

createAdHocNetwork()
{
    echo "Creating Hotspot"
    ip link set dev "$wifidev" down
    ip a add 192.168.4.1/24 brd + dev "$wifidev"
    ip link set dev "$wifidev" up
    dhcpcd -k "$wifidev" >/dev/null 2>&1
    systemctl start dnsmasq
    systemctl start hostapd
}

KillHotspot()
{
    echo "Shutting Down Hotspot"
    ip link set dev "$wifidev" down
    systemctl stop hostapd
    systemctl stop dnsmasq
    ip addr flush dev "$wifidev"
    ip link set dev "$wifidev" up
    dhcpcd  -n "$wifidev" >/dev/null 2>&1
}

ChkWifiUp()
{
	echo "Checking WiFi connection ok"
        sleep 20 #give time for connection to be completed to router
	if ! wpa_cli -i "$wifidev" status | grep 'ip_address' >/dev/null 2>&1
        then #Failed to connect to wifi (check your wifi settings, password etc)
	       echo 'Wifi failed to connect, falling back to Hotspot.'
               wpa_cli terminate "$wifidev" >/dev/null 2>&1
	       createAdHocNetwork
	fi
}


FindSSID()
{
#Check to see what SSID's and MAC addresses are in range
ssidChk=('NoSSid')
i=0; j=0
until [ $i -eq 1 ] #wait for wifi if busy, usb wifi is slower.
do
        ssidreply=$((iw dev "$wifidev" scan ap-force | egrep "^BSS|SSID:") 2>&1) >/dev/null 2>&1
        echo "SSid's in range: " $ssidreply
        echo "Device Available Check try " $j
        if (($j >= 10)); then #if busy 10 times goto hotspot
                 echo "Device busy or unavailable 10 times, going to Hotspot"
                 ssidreply=""
                 i=1
	elif echo "$ssidreply" | grep "No such device (-19)" >/dev/null 2>&1; then
                echo "No Device Reported, try " $j
		NoDevice
        elif echo "$ssidreply" | grep "Network is down (-100)" >/dev/null 2>&1 ; then
                echo "Network Not available, trying again" $j
                j=$((j + 1))
                sleep 2
	elif echo "$ssidreplay" | grep "Read-only file system (-30)" >/dev/null 2>&1 ; then
		echo "Temporary Read only file system, trying again"
		j=$((j + 1))
		sleep 2
	elif ! echo "$ssidreply" | grep "resource busy (-16)"  >/dev/null 2>&1 ; then
               echo "Device Available, checking SSid Results"
		i=1
	else #see if device not busy in 2 seconds
                echo "Device unavailable checking again, try " $j
		j=$((j + 1))
		sleep 2
	fi
done

for ssid in "${ssidsmac[@]}"
do
     if (echo "$ssidreply" | grep "$ssid") >/dev/null 2>&1
     then
	      #Valid SSid found, passing to script
              echo "Valid SSID Detected, assesing Wifi status"
              ssidChk=$ssid
              return 0
      else
	      #No Network found, NoSSid issued"
              echo "No SSid found, assessing WiFi status"
              ssidChk='NoSSid'
     fi
done
}

NoDevice()
{
	#if no wifi device,ie usb wifi removed, activate wifi so when it is
	#reconnected wifi to a router will be available
	echo "No wifi device connected"
	wpa_supplicant -B -i "$wifidev" -c /etc/wpa_supplicant/wpa_supplicant.conf >/dev/null 2>&1
	exit 1
}

FindSSID

#Create Hotspot or connect to valid wifi networks
if [ "$ssidChk" != "NoSSid" ]
then
       if systemctl status hostapd | grep "(running)" >/dev/null 2>&1
       then #hotspot running and ssid in range
              KillHotspot
              echo "Hotspot Deactivated, Bringing Wifi Up"
              wpa_supplicant -B -i "$wifidev" -c /etc/wpa_supplicant/wpa_supplicant.conf >/dev/null 2>&1
              ChkWifiUp
       elif { wpa_cli -i "$wifidev" status | grep 'ip_address'; } >/dev/null 2>&1
       then #Already connected
              echo "Wifi already connected to a network"
       else #ssid exists and no hotspot running connect to wifi network
              echo "Connecting to the WiFi Network"
              wpa_supplicant -B -i "$wifidev" -c /etc/wpa_supplicant/wpa_supplicant.conf >/dev/null 2>&1
              ChkWifiUp
       fi
else #ssid or MAC address not in range
       if systemctl status hostapd | grep "(running)" >/dev/null 2>&1
       then
              echo "Hostspot already active"
       elif { wpa_cli status | grep "$wifidev"; } >/dev/null 2>&1
       then
              echo "Cleaning wifi files and Activating Hotspot"
              wpa_cli terminate >/dev/null 2>&1
              ip addr flush "$wifidev"
              ip link set dev "$wifidev" down
              rm -r /var/run/wpa_supplicant >/dev/null 2>&1
              createAdHocNetwork
       else #"No SSID, activating Hotspot"
              createAdHocNetwork
       fi
fi
EOF

chmod +x /usr/bin/autohotspot

cat << 'EOF' > /etc/rc.local
#!/bin/sh -e
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#
# By default this script does nothing.

# Print the IP address
_IP=$(hostname -I) || true
if [ "$_IP" ]; then
  printf "My IP address is %s\n" "$_IP"
fi
MAC=`ip l show wlan0 | grep ether | cut -d ' ' -f 6 | sed 's/://g'`
sed -i "/^ssid/c\ssid=WF-$MAC" /etc/hostapd/hostapd.conf
/usr/bin/autohotspot
exit 0
EOF

cat << 'EOF' > /usr/bin/wonderfoonupdate
#!/bin/bash

wget -q --spider http://google.com

if [ $? -eq 0 ]; then
    echo "Online"
    echo "Updating Wonderfoon-python"
    cd /home/pi/wonderfoon-python
    git pull
    echo "Updating Wonderfoon-webinterface"
    cd /home/pi/wonderfoon-webinterface
    git pull
else
    echo "Offline"
fi
EOF

chmod +x /usr/bin/wonderfoonupdate

echo "*/5 * * * * root /usr/bin/autohotspot >/dev/null 2>&1" >> /etc/crontab
echo "*/5 * * * * root /usr/bin/wonderfoonupdate >/dev/null 2>&1" >> /etc/crontab

# Cleanup...
unset HISTFILE
> /root/.bash_history
> /home/pi/.bash_history

#Starting up...
systemctl daemon-reload
systemctl enable wonderfoon-python.service
systemctl enable wonderfoon-webinterface.service
systemctl unmask hostapd
systemctl unmask dnsmasq
systemctl disable hostapd
systemctl disable dnsmasq

echo "Done! Rebooting..."
reboot