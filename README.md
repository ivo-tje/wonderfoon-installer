# wonderfoon-installer

Wrapper voor wonderfoon-python en wonderfoon-webinterface om ze in 1x te installeren


Neem een schone raspbian-lite installatie.
Log in via console of ssh*

```
sudo apt -y install git
git clone https://gitlab.com/ivo-tje/wonderfoon-installer.git
cd wonderfoon-installer
sudo bash install.sh
```


Om in te loggen met ssh, maak in het boot volume een lege file met SSH en zorg dat er een wpa_supplicant.conf is om met je WiFi netwerk te verbinden

```
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1

network={
       ssid="Mijn-WiFi"
       psk="Mijn Wachtwoord"
       key_mgmt=WPA-PSK
}
```

[![ko-fi](https://www.ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/W7W12Q4NZ)
